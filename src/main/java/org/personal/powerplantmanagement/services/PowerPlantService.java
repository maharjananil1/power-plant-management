package org.personal.powerplantmanagement.services;

import org.personal.powerplantmanagement.common.dao.Filter;
import org.personal.powerplantmanagement.common.service.Service;
import org.personal.powerplantmanagement.domain.PowerPlantDomain;
import org.personal.powerplantmanagement.domain.PowerPlantSummaryRecord;

import java.util.List;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
public interface PowerPlantService extends Service<PowerPlantDomain> {
    PowerPlantSummaryRecord fetchAllPowerPlant(Filter filter);

    String saveBulk(List<PowerPlantDomain> domains);
}
