package org.personal.powerplantmanagement.services;

import lombok.AllArgsConstructor;
import org.personal.powerplantmanagement.common.dao.Filter;
import org.personal.powerplantmanagement.dao.PowerPlantConverter;
import org.personal.powerplantmanagement.dao.PowerPlantDao;
import org.personal.powerplantmanagement.dao.PowerPlantFilter;
import org.personal.powerplantmanagement.dao.PowerPlantNameAndCapacity;
import org.personal.powerplantmanagement.domain.PowerPlantDomain;
import org.personal.powerplantmanagement.domain.PowerPlantSummaryRecord;
import org.personal.powerplantmanagement.utils.DateUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The type Power plant service.
 *
 * @author maharjananil
 * @Date 04 /28/2023
 */
@Service
@AllArgsConstructor
public class PowerPlantServiceImpl implements PowerPlantService {
    private final PowerPlantDao powerPlantDao;

    @Override
    public String add(PowerPlantDomain domain) {
        domain.validate();
        powerPlantDao.save(new PowerPlantConverter().toEntity(domain));
        return DateUtils.now();
    }

    @Override
    public String saveBulk(List<PowerPlantDomain> domains) {
        domains.forEach(PowerPlantDomain::validate);
        powerPlantDao.saveAll(new PowerPlantConverter().toEntities(domains));
        return DateUtils.now();
    }

    @Override
    public PowerPlantSummaryRecord fetchAllPowerPlant(Filter filter) {
        PowerPlantFilter powerPlantFilter = (PowerPlantFilter) filter;
        powerPlantFilter.validate();
        List<PowerPlantNameAndCapacity> entities = powerPlantDao.findAllByPostCodeRange(powerPlantFilter.getFrom(), powerPlantFilter.getTo());
        AtomicInteger totalCapacity = new AtomicInteger();
        List<String> names = new ArrayList<>(entities.size());
        entities.forEach(entity -> {
            names.add(entity.getName());
            totalCapacity.addAndGet(entity.getCapacity());
        });
        return new PowerPlantSummaryRecord(names, totalCapacity.get(), (float) totalCapacity.get() / entities.size());
    }
}
