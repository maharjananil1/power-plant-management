package org.personal.powerplantmanagement.utils;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
public class StringUtils {
    private StringUtils() {}

    public static boolean isNullOrBlank(String value) {
        return value == null || value.trim().length() == 0;
    }
}
