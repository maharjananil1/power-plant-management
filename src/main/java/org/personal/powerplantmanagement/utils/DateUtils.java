package org.personal.powerplantmanagement.utils;

import java.time.LocalDateTime;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
public class DateUtils {
    private DateUtils() {
    }

    public static String now() {
        return LocalDateTime.now().toString();
    }
}
