package org.personal.powerplantmanagement.dao;

import org.personal.powerplantmanagement.entities.PowerPlantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Repository
public interface PowerPlantDao extends JpaRepository<PowerPlantEntity,String> {
    @Query(nativeQuery = true, value = "SELECT name,capacity FROM power_plant WHERE postcode BETWEEN ?1 AND ?2 ORDER BY name ASC")
    List<PowerPlantNameAndCapacity> findAllByPostCodeRange(String from, String to);
}
