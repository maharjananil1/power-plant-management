package org.personal.powerplantmanagement.dao;

import org.personal.powerplantmanagement.common.dao.EntityConverter;
import org.personal.powerplantmanagement.domain.PowerPlantDomain;
import org.personal.powerplantmanagement.entities.PowerPlantEntity;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
public class PowerPlantConverter implements EntityConverter<PowerPlantEntity, PowerPlantDomain> {
    public PowerPlantDomain toDomain(PowerPlantEntity entity) {
        PowerPlantDomain domain = new PowerPlantDomain();
        domain.setId(entity.getId());
        domain.setName(entity.getName());
        domain.setPostcode(entity.getPostcode());
        domain.setCapacity(entity.getCapacity());
        return domain;
    }

    public PowerPlantEntity toEntity(PowerPlantDomain domain) {
        PowerPlantEntity entity = new PowerPlantEntity();
        entity.setId(domain.getId());
        entity.setName(domain.getName());
        entity.setPostcode(domain.getPostcode());
        entity.setCapacity(domain.getCapacity());
        return entity;
    }
}
