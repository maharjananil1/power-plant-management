package org.personal.powerplantmanagement.dao;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
public interface PowerPlantNameAndCapacity {
    String getName();

    int getCapacity();
}
