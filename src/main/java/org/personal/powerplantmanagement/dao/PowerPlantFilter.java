package org.personal.powerplantmanagement.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.personal.powerplantmanagement.common.dao.Filter;
import org.personal.powerplantmanagement.common.errors.PowerPlantErrorCode;
import org.personal.powerplantmanagement.common.exception.ApiRequestException;
import org.personal.powerplantmanagement.utils.StringUtils;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PowerPlantFilter implements Filter {
    private String from;
    private String to;

    public void validate() {
        if (StringUtils.isNullOrBlank(from))
            throw new ApiRequestException(PowerPlantErrorCode.POST_CODE_FROM_MISSING.getCode(), PowerPlantErrorCode.POST_CODE_FROM_MISSING.getMessage());
        if (StringUtils.isNullOrBlank(to))
            throw new ApiRequestException(PowerPlantErrorCode.POST_CODE_TO_MISSING.getCode(), PowerPlantErrorCode.POST_CODE_TO_MISSING.getMessage());
    }
}
