package org.personal.powerplantmanagement.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.personal.powerplantmanagement.common.entity.AbstractEntity;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "power_plant")
public class PowerPlantEntity extends AbstractEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private String name;
    private String postcode;
    private int capacity;
}
