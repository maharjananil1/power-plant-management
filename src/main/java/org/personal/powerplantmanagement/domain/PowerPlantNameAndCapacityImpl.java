package org.personal.powerplantmanagement.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.personal.powerplantmanagement.dao.PowerPlantNameAndCapacity;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PowerPlantNameAndCapacityImpl implements PowerPlantNameAndCapacity {
    private String name;
    private int capacity;
}
