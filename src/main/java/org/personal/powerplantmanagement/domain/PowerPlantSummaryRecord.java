package org.personal.powerplantmanagement.domain;

import java.util.List;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
public record PowerPlantSummaryRecord(List<String> names, int totalCapacity, float averageCapacity) {

}
