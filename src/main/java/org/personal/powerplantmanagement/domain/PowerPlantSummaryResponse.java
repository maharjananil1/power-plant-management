package org.personal.powerplantmanagement.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.personal.powerplantmanagement.common.responses.ResponseCode;
import org.personal.powerplantmanagement.utils.DateUtils;
import org.springframework.http.ResponseEntity;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PowerPlantSummaryResponse {
    private String timestamp;
    private String code;
    private String message;
    private PowerPlantSummaryRecord data;

    public static ResponseEntity<PowerPlantSummaryResponse> ok(PowerPlantSummaryRecord data) {
        return ResponseEntity.ok(PowerPlantSummaryResponse.builder()
                .timestamp(DateUtils.now())
                .code(ResponseCode.OK.getCode())
                .message(ResponseCode.OK.getMessage())
                .data(data)
                .build());
    }
}