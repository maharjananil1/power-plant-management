package org.personal.powerplantmanagement.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.personal.powerplantmanagement.common.domain.AbstractDomain;
import org.personal.powerplantmanagement.common.errors.PowerPlantErrorCode;
import org.personal.powerplantmanagement.common.exception.ApiRequestException;
import org.personal.powerplantmanagement.utils.StringUtils;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PowerPlantDomain extends AbstractDomain {
    @JsonIgnore
    private String id;
    private String name;
    private String postcode;
    private int capacity;


    @Override
    public void validate() {
        if (StringUtils.isNullOrBlank(name))
            throw new ApiRequestException(PowerPlantErrorCode.NAME_MISSING.getCode(), PowerPlantErrorCode.NAME_MISSING.getMessage());
        if (StringUtils.isNullOrBlank(postcode))
            throw new ApiRequestException(PowerPlantErrorCode.POST_CODE_MISSING.getCode(), PowerPlantErrorCode.POST_CODE_MISSING.getMessage());
        if (capacity <= 0)
            throw new ApiRequestException(PowerPlantErrorCode.CAPACITY_INVALID.getCode(), PowerPlantErrorCode.CAPACITY_INVALID.getMessage());
    }
}