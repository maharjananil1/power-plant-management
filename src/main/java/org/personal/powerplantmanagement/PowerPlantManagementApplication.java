package org.personal.powerplantmanagement;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Power Plant Management", version = "v1.0"))
public class PowerPlantManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(PowerPlantManagementApplication.class, args);
    }

}
