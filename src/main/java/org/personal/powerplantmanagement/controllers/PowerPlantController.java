package org.personal.powerplantmanagement.controllers;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.personal.powerplantmanagement.common.responses.AddResponse;
import org.personal.powerplantmanagement.dao.PowerPlantFilter;
import org.personal.powerplantmanagement.domain.PowerPlantDomain;
import org.personal.powerplantmanagement.domain.PowerPlantSummaryResponse;
import org.personal.powerplantmanagement.services.PowerPlantService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@RestController
@RequestMapping(value = "power-plant")
@AllArgsConstructor
@Tag(name = "Power Plant", description = "Power plant management APIs")
public class PowerPlantController {
    private final PowerPlantService service;

    @PostMapping(path = "/bulk")
    @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = AddResponse.class), mediaType = "application/json")})
    public ResponseEntity<AddResponse> addPowerPlantBulk(@RequestBody List<PowerPlantDomain> plantDomains) {
        return AddResponse.ok(service.saveBulk(plantDomains));
    }

    @PostMapping
    @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = AddResponse.class), mediaType = "application/json")})
    public ResponseEntity<AddResponse> addPowerPlant(@RequestBody PowerPlantDomain plantDomain) {
        return AddResponse.ok(service.add(plantDomain));
    }

    @PostMapping(path = "/all")
    @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = PowerPlantSummaryResponse.class), mediaType = "application/json")})
    public ResponseEntity<PowerPlantSummaryResponse> fetchAll(@RequestBody PowerPlantFilter filter) {
        return PowerPlantSummaryResponse.ok(service.fetchAllPowerPlant(filter));
    }
}
