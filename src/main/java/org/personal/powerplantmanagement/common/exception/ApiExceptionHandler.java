package org.personal.powerplantmanagement.common.exception;

import org.personal.powerplantmanagement.common.responses.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@ControllerAdvice
public class ApiExceptionHandler {
    public ResponseEntity<ErrorResponse> handleApiRequestException(ApiRequestException ex) {
        return new ResponseEntity<>(ErrorResponse.error(ex.getErrorCode(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
