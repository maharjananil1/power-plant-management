package org.personal.powerplantmanagement.common.exception;

import lombok.Getter;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Getter
public class ApiRequestException extends RuntimeException {
    private final String errorCode;

    public ApiRequestException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public ApiRequestException(String errorCode, Throwable cause, String message) {
        super(message, cause);
        this.errorCode = errorCode;
    }
}
