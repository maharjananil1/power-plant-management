package org.personal.powerplantmanagement.common.service;

import org.personal.powerplantmanagement.common.domain.Domain;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
public interface Service<T extends Domain> {
    String add(T domain);
}
