package org.personal.powerplantmanagement.common.entity;

import lombok.Data;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Data
public abstract class AbstractEntity implements Entity {
    private String createdOn;
    private String lastModifiedOn;
    private String deletedOn;
    private boolean active;
    private boolean deleted;
}
