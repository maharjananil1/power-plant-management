package org.personal.powerplantmanagement.common.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Data
public abstract class AbstractDomain implements Domain {
    @JsonIgnore
    private String createdOn;
    @JsonIgnore
    private String lastModifiedOn;
    @JsonIgnore
    private String deletedOn;
    @JsonIgnore
    private boolean active;
    @JsonIgnore
    private boolean deleted;

    protected abstract void validate();
}
