package org.personal.powerplantmanagement.common.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.personal.powerplantmanagement.utils.DateUtils;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
    private String timestamp;
    private String code;
    private String message;

    public static ErrorResponse error(String code, String message) {
        return ErrorResponse.builder()
                .timestamp(DateUtils.now())
                .code(code)
                .message(message)
                .build();
    }
}
