package org.personal.powerplantmanagement.common.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Getter
@AllArgsConstructor
public enum ResponseCode {
    OK("0", "SUCCESS");
    private final String code;
    private final String message;
}
