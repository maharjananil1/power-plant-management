package org.personal.powerplantmanagement.common.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.personal.powerplantmanagement.utils.DateUtils;
import org.springframework.http.ResponseEntity;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddResponse {
    private String timestamp;
    private String code;
    private String message;
    private String data;

    public static ResponseEntity<AddResponse> ok(String data) {
        return ResponseEntity.ok(AddResponse.builder()
                .timestamp(DateUtils.now())
                .code(ResponseCode.OK.getCode())
                .message(ResponseCode.OK.getMessage())
                .data(data)
                .build());
    }
}
