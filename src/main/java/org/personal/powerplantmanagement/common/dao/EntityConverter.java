package org.personal.powerplantmanagement.common.dao;

import org.personal.powerplantmanagement.common.domain.Domain;
import org.personal.powerplantmanagement.common.entity.Entity;

import java.util.List;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
public interface EntityConverter<E extends Entity, D extends Domain> {
    D toDomain(E entity);

    E toEntity(D domain);


    default List<D> toDomains(List<E> entities) {
        return entities.stream().map(this::toDomain).toList();
    }

    default List<E> toEntities(List<D> domains) {
        return domains.stream().map(this::toEntity).toList();
    }
}