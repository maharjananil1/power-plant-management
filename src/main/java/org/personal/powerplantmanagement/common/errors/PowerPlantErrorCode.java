package org.personal.powerplantmanagement.common.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Getter
@AllArgsConstructor
public enum PowerPlantErrorCode {
    NAME_MISSING("PPE1001", "Name missing/"),
    CAPACITY_INVALID("PPE1002", "Invalid capacity."),
    POST_CODE_MISSING("PPE1003", "Postcode missing."),
    POST_CODE_FROM_MISSING("PPE1004", "Postcode from missing."),
    POST_CODE_TO_MISSING("PPE1005", "Postcode to missing."),
    ;
    private final String code;
    private final String message;
}