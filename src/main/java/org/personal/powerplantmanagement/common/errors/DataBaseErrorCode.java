package org.personal.powerplantmanagement.common.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@Getter
@AllArgsConstructor
public enum DataBaseErrorCode {
    ENTITY_CREATION_FAILED("DBE1001", "Entity creation failed."),

    ;
    private final String code;
    private final String message;
}
