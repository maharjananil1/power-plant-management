package org.personal.powerplantmanagement.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
class StringUtilsTest {
    @Test
    @DisplayName("Should return true if value is null")
    void isNullTest() {
        assertTrue(StringUtils.isNullOrBlank(null));
    }

    @Test
    @DisplayName("Should return true if value is blank")
    void isBlankTest() {
        assertTrue(StringUtils.isNullOrBlank(""));
    }

    @Test
    @DisplayName("Should return false if value is not null and blank")
    void isNulOrBlankTest() {
        assertFalse(StringUtils.isNullOrBlank("Hello"));
    }

}