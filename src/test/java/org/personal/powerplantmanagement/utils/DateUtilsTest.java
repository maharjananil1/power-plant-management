package org.personal.powerplantmanagement.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
class DateUtilsTest {
    @Test
    void nowTest(){
        Assertions.assertTrue(LocalDateTime.now().isBefore(LocalDateTime.parse(DateUtils.now())));
    }
}
