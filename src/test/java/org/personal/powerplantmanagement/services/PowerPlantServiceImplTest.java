package org.personal.powerplantmanagement.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.personal.powerplantmanagement.dao.PowerPlantDao;
import org.personal.powerplantmanagement.dao.PowerPlantFilter;
import org.personal.powerplantmanagement.dao.PowerPlantNameAndCapacity;
import org.personal.powerplantmanagement.domain.PowerPlantDomain;
import org.personal.powerplantmanagement.domain.PowerPlantNameAndCapacityImpl;
import org.personal.powerplantmanagement.domain.PowerPlantSummaryRecord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
class PowerPlantServiceImplTest {
    @Mock
    private PowerPlantDao powerPlantDao;
    private AutoCloseable autoCloseable;
    private PowerPlantService powerPlantService;

    @BeforeEach
    void setUp() {
        this.autoCloseable = MockitoAnnotations.openMocks(this);
        powerPlantService = new PowerPlantServiceImpl(powerPlantDao);
    }

    @AfterEach
    void tearDown() throws Exception {
        this.autoCloseable.close();
    }

    @Test
    @DisplayName("Should check if add is successful")
    void addTest() {
        this.powerPlantService.add(this.mockData());
        verify(powerPlantDao, times(1)).save(any());
    }

    @Test
    @DisplayName("Should check if bulk add is successful")
    void saveBulkTest() {
        this.powerPlantService.saveBulk(Collections.singletonList(this.mockData()));
        verify(powerPlantDao, times(1)).saveAll(any());
    }


    @Test
    @DisplayName("Should throw exception if postcode filter from is missing")
    void fetchAllPowerPlantPostcodeFromBlankTest() {
        when(powerPlantDao.findAllByPostCodeRange("0820", "0880")).thenReturn(this.mockDatabaseResponse());
        PowerPlantSummaryRecord response = this.powerPlantService.fetchAllPowerPlant(PowerPlantFilter.builder().from("0820").to("0880").build());
        assertEquals(2, response.names().size());
        assertEquals(40500, response.totalCapacity());
        assertEquals(20250, response.averageCapacity());
        verify(powerPlantDao, times(1)).findAllByPostCodeRange(any(), any());
    }

    private PowerPlantDomain mockData() {
        return PowerPlantDomain.builder()
                .name("name")
                .postcode("postcode")
                .capacity(1)
                .build();
    }

    private List<PowerPlantNameAndCapacity> mockDatabaseResponse() {
        List<PowerPlantNameAndCapacity> powerPlantEntities = new ArrayList<>(2);
        PowerPlantNameAndCapacityImpl ob1 = PowerPlantNameAndCapacityImpl.builder().name("Yirrkala").capacity(13500).build();
        PowerPlantNameAndCapacityImpl ob2 = PowerPlantNameAndCapacityImpl.builder().name("Bagot").capacity(27000).build();
        powerPlantEntities.add(ob1);
        powerPlantEntities.add(ob2);
        return powerPlantEntities;
    }
}