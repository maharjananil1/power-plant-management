package org.personal.powerplantmanagement.dao;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.personal.powerplantmanagement.common.errors.PowerPlantErrorCode;
import org.personal.powerplantmanagement.common.exception.ApiRequestException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
class PowerPlantFilterTest {

    @Test
    @DisplayName("Should throw exception if from is blank or null")
    void validateFromTest() {
        PowerPlantFilter filter = new PowerPlantFilter();
        filter.setFrom("");
        Exception exception = assertThrows(ApiRequestException.class,filter::validate);
        assertEquals(PowerPlantErrorCode.POST_CODE_FROM_MISSING.getMessage(),exception.getMessage());
    }

    @Test
    @DisplayName("Should throw exception if to is blank or null")
    void validateToTest() {
        PowerPlantFilter filter = new PowerPlantFilter();
        filter.setFrom("from");
        filter.setTo("");
        Exception exception = assertThrows(ApiRequestException.class,filter::validate);
        assertEquals(PowerPlantErrorCode.POST_CODE_TO_MISSING.getMessage(),exception.getMessage());
    }
}