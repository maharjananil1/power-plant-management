package org.personal.powerplantmanagement.dao;

import org.junit.jupiter.api.Test;
import org.personal.powerplantmanagement.entities.PowerPlantEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
@DataJpaTest
class PowerPlantDaoTest {
    @Autowired
    private PowerPlantDao powerPlantDao;

    @Test
    void findAllByPostCodeRange() {
        powerPlantDao.saveAll(this.mockData());
        List<PowerPlantNameAndCapacity> data = powerPlantDao.findAllByPostCodeRange("0820","0880");
        assertEquals(2,data.size());
    }

    private List<PowerPlantEntity> mockData(){
        List<PowerPlantEntity> powerPlantEntities = new ArrayList<>(5);
        powerPlantEntities.add(PowerPlantEntity.builder().name("Yirrkala").postcode("0880").capacity(13500).build());
        powerPlantEntities.add(PowerPlantEntity.builder().name("Bagot").postcode("0820").capacity(2700).build());
        powerPlantEntities.add(PowerPlantEntity.builder().name("Werrington County").postcode("2747").capacity(13500).build());
        powerPlantEntities.add(PowerPlantEntity.builder().name("Akunda Bay").postcode("2084").capacity(13500).build());
        powerPlantEntities.add(PowerPlantEntity.builder().name("Kent Town").postcode("5067").capacity(13500).build());
        return powerPlantEntities;
    }
}