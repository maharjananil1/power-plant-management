package org.personal.powerplantmanagement.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.personal.powerplantmanagement.common.errors.PowerPlantErrorCode;
import org.personal.powerplantmanagement.common.exception.ApiRequestException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author maharjananil
 * @Date 04/28/2023
 */
class PowerPlantDomainTest {

    @Test
    @DisplayName("Should throw exception if name is null")
    void validateNameNullTest() {
        PowerPlantDomain domain = this.mockData();
        domain.setName(null);
        Exception exception = assertThrows(ApiRequestException.class, domain::validate);
        assertEquals(
                PowerPlantErrorCode.NAME_MISSING.getMessage(),
                exception.getMessage());
    }

    @Test
    @DisplayName("Should throw exception if name is blank")
    void validateNameBlankTest() {
        PowerPlantDomain domain = this.mockData();
        domain.setName("");
        Exception exception = assertThrows(ApiRequestException.class, domain::validate);
        assertEquals(
                PowerPlantErrorCode.NAME_MISSING.getMessage(),
                exception.getMessage());
    }

    @Test
    @DisplayName("Should throw exception if postcode is null")
    void validatePostcodeNullTest() {
        PowerPlantDomain domain = this.mockData();
        domain.setPostcode(null);
        Exception exception = assertThrows(ApiRequestException.class, domain::validate);
        assertEquals(
                PowerPlantErrorCode.POST_CODE_MISSING.getMessage(),
                exception.getMessage());
    }

    @Test
    @DisplayName("Should throw exception if postcode is blank")
    void validatePostcodeBlankTest() {
        PowerPlantDomain domain = this.mockData();
        domain.setPostcode("");
        Exception exception = assertThrows(ApiRequestException.class, domain::validate);
        assertEquals(
                PowerPlantErrorCode.POST_CODE_MISSING.getMessage(),
                exception.getMessage());
    }

    @Test
    @DisplayName("Should throw exception if capacity is invalid")
    void validateCapacityTest() {
        PowerPlantDomain domain = this.mockData();
        domain.setCapacity(-1);
        Exception exception = assertThrows(ApiRequestException.class, domain::validate);
        assertEquals(
                PowerPlantErrorCode.CAPACITY_INVALID.getMessage(),
                exception.getMessage());
    }

    private PowerPlantDomain mockData(){
        return PowerPlantDomain.builder()
                .name("name")
                .postcode("postcode")
                .capacity(1)
                .build();
    }
}