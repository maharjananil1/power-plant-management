# Power Plant Management

## Description

Power plant system for aggregating distributed power sources into a single cloud based energy provider. This application
provides REST Apis to manage the functionality of power plant.

## Built with

- Spring Boot 3.0.6
- Java 17
- Maven 3.8.4
- H2

## Dependencies

- Lombok
- Spring doc

## Instructions

- mvn spring-boot:run
    - Start project
- Setup sonarqube
  - Download and install Java 17 on your system. 
  - Download the SonarQube Community Edition zip file.
  - unzip it in, for example, C:\sonarqube or /opt/sonarqube
  - Run sonarqube
    - On Windows, execute:{location of sonarqube extracted}\sonarqube\bin\windows-x86-64\StartSonar.bat
    - On other operating systems, as a non-root user execute: {location of sonarqube extracted}/sonarqube/bin/<OS>/sonar.sh console
- mvn clean verify sonar:sonar \ -Dsonar.projectKey=plant \ -Dsonar.projectName='plant' \
  -Dsonar.host.url=http://localhost:9000 \ -Dsonar.token=generated token
    - Runs sonarqube to scan the project for bugs, smelly codes, vulnerability

## Links

- http://localhost:8080/api/v1/swagger-ui/index.html#/
    - Open api ui for api documentation
- http://localhost:8080/api/v1/power-plant/bulk
    - POST method to add list of batteries
    - Request Body
      [{"name": "Cannington","postcode": "6107","capacity": 13500 }]
    - Response Body
      {"timestamp": "2023-04-28T11:35:39.974944","code": "0","message": "SUCCESS","data": "2023-04-28T11:35:39.974783"}
- http://localhost:8080/api/v1/power-plant
    - POST method to add battery info
    - Request Body
      {"name": "Cannington","postcode": "6107","capacity": 13500 }
    - Response Body
      {"timestamp": "2023-04-28T11:35:39.974944","code": "0","message": "SUCCESS","data": "2023-04-28T11:35:39.974783"}`
- http://localhost:8080/api/v1/power-plant/all
  - POST method to fetch names of batteries, total capacity, average capacity
  - Request Body
    {"from":"","to":""}
  - Response Body
    {"timestamp": "2023-04-28T14:27:29.195907","code": "0","message": "SUCCESS","data": {"names": ["Bagot","Yirrkala"],"totalCapacity": 40500,"averageCapacity": 20250.0 } }
- http://localhost:9000/project/issues?id=<project_id>&resolved=false
  - Sonarqube report

## Author

**Anil Maharjan**

- [Profile](https://gitlab.com/maharjananil1)
- [Email](mailto:maharjananil1@gmail.com)
